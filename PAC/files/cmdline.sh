#!/bin/bash

cat /boot/cmdline.txt | grep 'cgroup_enable'
if [ $(echo $?) == 0 ]
then
        exit 0
else
         echo $(sed '1s/$/ cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1/' /boot/cmdline.txt) > /boot/cmdline.txt
fi
