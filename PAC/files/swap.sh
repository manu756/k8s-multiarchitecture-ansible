#!/bin/bash

systemctl status dphys-swapfile

if [ $(echo $?) != 0 ]
then
        exit 0
else
        systemctl stop dphys-swapfile && apt purge dphys-swapfile -y && swapoff -a
fi
